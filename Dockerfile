FROM maven:3.8.4-jdk-11 AS build
WORKDIR /app
COPY . .
RUN mvn clean install

FROM openjdk:11-jre-slim
WORKDIR /app
COPY --from=build /app/target/docker-java-sample-webapp-1.0-SNAPSHOT.war /app/
EXPOSE 8080
CMD ["java", "-jar", "docker-java-sample-webapp-1.0-SNAPSHOT.war"]